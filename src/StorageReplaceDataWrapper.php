<?php

namespace Drupal\config_partial_import;

use Drupal\config\StorageReplaceDataWrapper as DrupalStorageReplaceDataWrapper;
use Drupal\Core\Config\FileStorage;

/**
 * Wraps a configuration storage to allow replacing specific configuration data.
 */
class StorageReplaceDataWrapper extends DrupalStorageReplaceDataWrapper {

  /**
   * Source path.
   *
   * @var string
   */
  protected string $sourcePath;

  /**
   * {@inheritdoc}
   */
  public function createCollection($collection) {
    $translation_source_storage_dir = $this->sourcePath . '/' . str_replace(".", "/", $collection);
    $source_storage = new FileStorage($translation_source_storage_dir);
    $names = $source_storage->listAll();
    if (empty($names)) {
      return new static(
        $this->storage->createCollection($collection),
        $collection
      );
    }
    $replacement_storage = parent::createCollection($collection);
    foreach ($names as $name) {
      $data = $source_storage->read($name);
      $replacement_storage->replaceData($name, $data);
    }
    return  $replacement_storage;
  }

  /**
   * Set source path.
   *
   * @param string $source_path
   *
   * @return void
   */
  public function setSourcePath(string $source_path) {
    $this->sourcePath = $source_path;
  }

  /**
   * {@inheritdoc}
   */
  public function getAllCollectionNames() {
    $collections = array();
    foreach (\Drupal::languageManager()->getLanguages() as $language) {
      $collections[] = 'language.' . $language->getId();
    }
    return array_unique(array_merge($this->storage->getAllCollectionNames(), $collections));
  }

}
