<?php

namespace Drupal\config_partial_import\Commands;

use Drupal\config_partial_import\StorageReplaceDataWrapper;
use Drupal\Core\Config\FileStorage;
use Drupal\Core\Config\StorageComparer;
use Drupal\Core\Config\StorageTransformerException;
use Drupal\Core\Site\Settings;
use Drush\Drupal\Commands\config\ConfigCommands;
use Drush\Drupal\Commands\config\ConfigImportCommands;
use Drush\Exceptions\UserAbortException;

/**
 * Class to import single files into config.
 *
 * @package Drupal\config_partial_import\Commands
 */
class ConfigPartialImportCommands extends ConfigImportCommands {

  /**
   * Import config from a config directory.
   *
   * @command config_partial_import
   *
   *
   * @return bool|void
   * @option diff Show preview as a diff.
   * @option source An arbitrary directory that holds the configuration files.
   * @option partial Allows for partial config imports from the source directory. Only updates and new configs will be processed with this flag (missing configs will not be deleted). No config transformation happens.
   * @aliases cpim,config-partial-import
   * @topics docs:deploy
   * @bootstrap full
   *
   * @throws StorageTransformerException
   * @throws UserAbortException
   */
  public function import(array $options = ['source' => self::REQ, 'partial' => false, 'diff' => false]) {
    // Determine source directory.
    $source_storage_dir = ConfigCommands::getDirectory($options['source']);

    // Prepare the configuration storage for the import.
    if ($source_storage_dir == \Webmozart\PathUtil\Path::canonicalize(Settings::get('config_sync_directory'))) {
      $source_storage = $this->getConfigStorageSync();
    } else {
      $source_storage = new FileStorage($source_storage_dir);
    }

    // Determine $source_storage in partial case.
    $active_storage = $this->getConfigStorage();
    if ($options['partial']) {
      $replacement_storage = new StorageReplaceDataWrapper($active_storage);
      $replacement_storage->setSourcePath($source_storage_dir);
      foreach ($source_storage->listAll() as $name) {
        $data = $source_storage->read($name);
        $replacement_storage->replaceData($name, $data);
      }
      $source_storage = $replacement_storage;
    } elseif ($this->hasImportTransformer()) {
      // Use the import transformer if it is available. (Drupal ^8.8)
      // Drupal core does not apply transformations for single imports.
      // And in addition the StorageReplaceDataWrapper is not compatible
      // with StorageCopyTrait::replaceStorageContents.
      $source_storage = $this->getImportTransformer()->transform($source_storage);
    }

    $config_manager = $this->getConfigManager();
    $storage_comparer = new StorageComparer($source_storage, $active_storage, $config_manager);


    if (!$storage_comparer->createChangelist()->hasChanges()) {
      $this->logger()->notice(('There are no changes to import.'));
      return;
    }

    if (!$options['diff']) {
      $change_list = [];
      foreach ($storage_comparer->getAllCollectionNames() as $collection) {
        $change_list[$collection] = $storage_comparer->getChangelist(null, $collection);
      }
      $table = ConfigCommands::configChangesTable($change_list, $this->output());
      $table->render();
    } else {
      $output = ConfigCommands::getDiff($active_storage, $source_storage, $this->output());

      $this->output()->writeln($output);
    }

    if (!$this->io()->confirm(dt('Import the listed configuration changes?'))) {
      throw new UserAbortException();
    }
    return drush_op([$this, 'doImport'], $storage_comparer);
  }

}
